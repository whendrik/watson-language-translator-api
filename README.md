# Watson Language Translator API

Example to call API with requests for translation customisation

Note the two steps mention in the [Documentation](https://cloud.ibm.com/docs/language-translator?topic=language-translator-customizing)

1. Customize with at least one `parallel corpus` file. You can upload multiple `parallel corpus` files with a single request. To successfully train with parallel corpora, all corpus files combined must contain at least 5000 parallel sentences. The cumulative size of all uploaded corpus files for a custom model is limited to 250 MB.

2. Customize the resulting model with a `forced glossary`. You can upload a single forced glossary file for a custom model. The size of a forced glossary for a custom model is limited to 10 MB. 